#!/usr/bin/python
import time
import os
import RPi.GPIO as GPIO
import glob
import datetime

#initialize the DS18B20
os.system("sudo modprobe w1-gpio")
os.system("sudo modprobe w1-therm")
#find the initialized device
devicedir = glob.glob("/sys/bus/w1/devices/10-*")
device = devicedir[0]+"/w1_slave"

#get values from the ds18b20
def getC () :
#open up the file
    f = open (device, 'r')
    sensor = f.readlines()
    f.close()
    
    #parse results from the file
    crc=sensor[0].split()[-1]
    temp=float(sensor[1].split()[-1].strip('t='))
    temp_C=(temp/1000.00)
    
    #output
    if 'YES' in crc:
            return temp_C
        else
        return 999.99

&nbsp;

GPIO.setmode(GPIO.BCM)
DEBUG = 1

# read SPI data from MCP3008 chip, 8 possible adc's (0 thru 7)
def readadc(adcnum, clockpin, mosipin, misopin, cspin):
if ((adcnum &gt; 7) or (adcnum &lt; 0)):
return -1
GPIO.output(cspin, True)

GPIO.output(clockpin, False) # start clock low
GPIO.output(cspin, False) # bring CS low

commandout = adcnum
commandout |= 0x18 # start bit + single-ended bit
commandout &lt;&lt;= 3 # we only need to send 5 bits here
for i in range(5):
if (commandout &amp; 0x80):
GPIO.output(mosipin, True)
else:
GPIO.output(mosipin, False)
commandout &lt;&lt;= 1
GPIO.output(clockpin, True)
GPIO.output(clockpin, False)

adcout = 0
# read in one empty bit, one null bit and 10 ADC bits
for i in range(12):
GPIO.output(clockpin, True)
GPIO.output(clockpin, False)
adcout &lt;&lt;= 1
if (GPIO.input(misopin)):
adcout |= 0x1

GPIO.output(cspin, True)

adcout /= 2 # first bit is 'null' so drop it
return adcout

# change these as desired - they're the pins connected from the
# SPI port on the ADC to the Cobbler
SPICLK = 18
SPIMISO = 23
SPIMOSI = 24
SPICS = 25

# set up the SPI interface pins
GPIO.setup(SPIMOSI, GPIO.OUT)
GPIO.setup(SPIMISO, GPIO.IN)
GPIO.setup(SPICLK, GPIO.OUT)
GPIO.setup(SPICS, GPIO.OUT)
# temperature sensor connected channel 0 of mcp3008
lm35_adcnum = 1
lm335_adcnum = 0
while True:
# read the analog pin (temperature sensor LM35)
read_lm35 = readadc(lm35_adcnum, SPICLK, SPIMOSI, SPIMISO, SPICS)
time.sleep(0.5)
read_lm335 = readadc(lm335_adcnum, SPICLK, SPIMOSI, SPIMISO, SPICS)

# convert analog reading to millivolts = ADC * ( 3300 / 1024 )
lm35mv = read_lm35 * ( 3300.00 / 1024.00)
lm335mv = (read_lm335-760) * ( 3300.00 / 1024.00)
# 10 mv per degree

lm35_C = (((lm35mv) / 10.00)-1.50) #LM35
lm335_C = ((lm335mv/10.00)-1.00) #LM335
ds18_C= getC()

# convert celsius to fahrenheit
lm35_F = ( lm35_C * 9.00 / 5.00 ) + 32.00
lm335_F = ( lm335_C * 9.00 / 5.00 ) + 32.00
ds18_F = ( ds18_C * 9.00 / 5.00 ) + 32.00

# remove decimal point from millivolts
lm35mv = "%d" % lm35mv
lm335mv = "%d" % lm335mv

# show two decimal place for temprature and voltage readings
lm35_C = "%.2f" % lm35_C
lm335_C = "%.2f" % lm335_C
ds18_C = "%.2f" % ds18_C

lm35_F = "%.2f" % lm35_F
lm335_F = "%.2f" % lm335_F
ds18_F = "%.2f" % ds18_F

if DEBUG:
print "DS18B20 :\t ADC: NA \t MV: NA \t C:",ds18_C,"\t F:",ds18_F
print "LM-35 :\t ADC:",read_lm35,"\t MV:",lm35mv,"\t C:",lm35_C,"\t F:",lm35_F
print "LM335 :\t ADC:",read_lm335,"\t MV:",lm335mv,"\t C:",lm335_C,"\t F:",lm335_F

#os.system("cat /sys/bus/w1/devices/28-*/w1_slave")
#print "temp_F:\t\t", temp_F
print "\n\n"
# hang out and do nothing for 10 seconds, avoid flooding cosm
time.sleep(5)
